package Vector3D;

import Vector3D.Normal3;


public class Vector3D {

	/**
	 * x-position of the vector.
	 */
	public final double x;

	/**
	 * y-position of the vector.
	 */
	public final double y;

	/**
	 * z-position of the vector.
	 */
	public final double z;

	/**
	 * the length of the vector.
	 */
	public final double magnitude;

	/**
	 * Creates a vector and calculates the magnitude.
	 *
	 * @param x x-position of the vector.
	 * @param y y-position of the vector.
	 * @param z z-position of the vector.
	 */
	public Vector3D(final double x, final double y, final double z) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.magnitude = Math.sqrt(Math.pow(this.x, 2.0) + Math.pow(this.y, 2.0) + Math.pow(this.z, 2.0));
	}

	/**
	 * Adds two vectors.
	 *
	 * @param v vector which should be added.
	 * @return returns a new Vector.
	 */
	public Vector3D add(final Vector3D v) {
		if (v == null) {
			throw new IllegalArgumentException("Vector cannot be null!");
		}
		final double x = this.x + v.x;
		final double y = this.y + v.y;
		final double z = this.z + v.z;
		return new Vector3D(x, y, z);
	}

	/**
	 * Adds a vector and a normal.
	 *
	 * @param n normal which should be added.
	 * @return returns a new vector.
	 */
	public Vector3D add(final Normal3 n) {
		if (n == null) {
			throw new IllegalArgumentException("Normal cannot be null!");
		}
		final double x = this.x + n.x;
		final double y = this.y + n.y;
		final double z = this.z + n.z;
		return new Vector3D(x, y, z);
	}

	/**
	 * Subtracts a vector with a normal.
	 *
	 * @param n normal which should be subtracted.
	 * @return returns a new vector.
	 */
	public Vector3D sub(final Normal3 n) {
		if (n == null) {
			throw new IllegalArgumentException("Normal cannot be null!");
		}
		final double x = this.x - n.x;
		final double y = this.y - n.y;
		final double z = this.z - n.z;
		return new Vector3D(x, y, z);
	}
	
	public Vector3D sub(final Vector3D n) {
		if (n == null) {
			throw new IllegalArgumentException("Normal cannot be null!");
		}
		final double x = this.x - n.x;
		final double y = this.y - n.y;
		final double z = this.z - n.z;
		return new Vector3D(x, y, z);
	}

	/**
	 * Multiplies a constant to a vector.
	 *
	 * @param c constant which should be multiplied.
	 * @return returns a new vector.
	 */
	public Vector3D mul(final double c) {
		return new Vector3D((this.x * c), (this.y * c), (this.z * c));
	}

	/**
	 * Calculates the dot-product of two vectors.
	 *
	 * @param v vector with which the dot-product should be calculated.
	 * @return returns the value of the dot-product.
	 */
	public double dot(final Vector3D v) {
		if (v == null) {
			throw new IllegalArgumentException("Vector cannot be null!");
		}
		return (this.x * v.x) + (this.y * v.y) + (this.z * v.z);
	}

	/**
	 * Calculates the dot-product of a vector and a normal.
	 *
	 * @param n normal with which the dot-product should be calculated.
	 * @return returns the value of the dot-product.
	 */
	public double dot(final Normal3 n) {
		if (n == null) {
			throw new IllegalArgumentException("Normal cannot be null!");
		}
		return (this.x * n.x) + (this.y * n.y) + (this.z * n.z);
	}

	/**
	 * Normalize a vector.
	 *
	 * @return returns a normalized vector.
	 */
	public Vector3D normalize() {
		return this.mul(1 / this.magnitude);
	}

	/**
	 * Change Vector to a Normal.
	 *
	 * @return a new normal.
	 */
	public Normal3 asNormal() {
		return new Normal3(this.x, this.y, this.z);
	}

	/**
	 * Reflects a vector on a normal.
	 *
	 * @param n the normal on which vector is reflected.
	 * @return returns a new reflected new vector
	 */
	public Vector3D reflectedOn(final Normal3 n) {
		if (n == null) {
			throw new IllegalArgumentException("Normal cannot be null!");
		}
		return new Vector3D(2 * this.dot(n) * n.x - this.x, 2 * this.dot(n) * n.y - this.y,
				2 * this.dot(n) * n.z - this.z);
	}

	/**
	 * Calculates the cross-product of two vectors.
	 *
	 * @param v vector with which the cross-product should be calculated.
	 * @return returns a new Vector.
	 */
	public Vector3D x(final Vector3D v) {
		if (v == null) {
			throw new IllegalArgumentException("Vector cannot be null!");
		}
        final double x = (this.y * v.z) - (this.z * v.y);
        final double y = (this.z * v.x) - (this.x * v.z);
        final double z = (this.x * v.y) - (this.y * v.x);
        return new Vector3D(x, y, z);
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(magnitude);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(z);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vector3D other = (Vector3D) obj;
		if (Double.doubleToLongBits(magnitude) != Double.doubleToLongBits(other.magnitude))
			return false;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		if (Double.doubleToLongBits(z) != Double.doubleToLongBits(other.z))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return ("Vector3 [x=" + x + ", y=" + y + ", z=" + z + ", magnitude=" + magnitude + "]");
	}
}
