package Vector3D;

/**
 * Describes a simple Normal
 */
public final class Normal3 {
	/**
	 * x-position of the normal
	 */
	public final double x;
	/**
	 * y-position of the normal
	 */
	public final double y;
	/**
	 * z-position of the normal
	 */
	public final double z;

	/**
	 * Creates a new normal.
	 *
	 * @param x x-position of the normal
	 * @param y y-position of the normal
	 * @param z z-position of the normal
	 */
	public Normal3(final double x, final double y, final double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/**
	 * Multiplies a normal with a constant.
	 *
	 * @param n constant with which the normal will be multiplied.
	 * @return returns a new normal with multiplied values.
	 */
	public Normal3 mul(final double n) {
		return new Normal3((this.x * n), (this.y * n), (this.z * n));
	}

	/**
	 * Adds a normal to a normal.
	 *
	 * @param n normal which should be added.
	 * @return new normal with added values from given normal.
	 */
	public Normal3 add(final Normal3 n) {
		if (n == null) {
			throw new IllegalArgumentException("Normal cannot be null!");
		}
		final double x = this.x + n.x;
		final double y = this.y + n.y;
		final double z = this.z + n.z;
		return new Normal3(x, y, z);
	}

	/**
	 * Creates the dot-product of a normal with a vector.
	 *
	 * @param v vector with which the dot-product should be created.
	 * @return returns the value of the dot-product
	 */
	public double dot(final Vector3D v) {
		if (v == null) {
			throw new IllegalArgumentException("Vector cannot be null!");
		}
		return (this.x * v.x) + (this.y * v.y) + (this.z * v.z);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(z);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Normal3 other = (Normal3) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		if (Double.doubleToLongBits(z) != Double.doubleToLongBits(other.z))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Normal3 [x=" + x + ", y=" + y + ", z=" + z + "]";
	}
}
