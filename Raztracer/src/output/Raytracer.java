package output;

import java.awt.Color;

import Vector3D.Normal3;
import Vector3D.Vector3D;
import camera.Camera;
import object3d.Object3D;
import world.World;

//Grafik in Java - Raytracing #5 - Teil 1 des Tracers
public class Raytracer {

Camera camera = new Camera();
	public void trace() {
		for (int i=0; i<Output.WIDTH;i++) {
			for (int j=0; j<Output.HEIGHT;j++) {
				double u = camera.l+ camera.r+(camera.r-camera.l)*(i+0.5)/Output.WIDTH;
				double v = camera.t+ camera.b+(camera.b-camera.t)*(j+0.5)/Output.HEIGHT;
				
				Vector3D s1 = camera.U.mul(u).add(camera.V.mul(v));
				
				// richtung wie wir gooken auf jeden pixel The direction we watch for every pixel.
				Vector3D s  = s1.add (camera.W_mal_d_negated);
				// direction normalized
				Vector3D direction = s.normalize();
				System.out.println("direction"+ direction.toString());
				System.out.println("eye"+ camera.eye.toString());
				Object3D intersect = null;
				double t = Double.MAX_VALUE -1;
				for (Object3D o: World.getworld().objects) {
					System.out.println("o"+ o.toString());
					double t2 = o.intersect (camera.eye,direction) ;
							if (t2>0&&t2<t) {
								System.out.println("t found");
							 intersect = o;
							 t =t2;
							
							}
				}
				if (intersect!=null) {
					System.out.println("intersect.color.getRGB()"+ intersect.color.getRGB());
					Output.setPixel(i, j,  intersect.color.getRGB());
					
				} 
				
			}
		}
		
	}

}
