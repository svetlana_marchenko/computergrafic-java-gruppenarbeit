package world;

import java.util.ArrayList;

import Vector3D.Vector3D;
import object3d.Object3D;
import object3d.Sphere;



public class World {
	
	public ArrayList<Object3D> objects = new  ArrayList<Object3D>() ;
	
	public static World world;
	
	public World() {
		
		objects.add(new Sphere(20, new Vector3D(0,0, 0)));
	}
	public static World getworld(){
		if (world==null) {
			World.world = new World();
		System.out.println("World done");
			
		}
		return world;
	} 
}
	