package object3d;



import java.awt.Color;

import Vector3D.Vector3D;




public abstract class Object3D {
	public final Color color;
	
	public Object3D(final Color color) {
		this.color=color; 
		}

	public abstract double intersect(Vector3D origin, Vector3D direction) ;
	

}
