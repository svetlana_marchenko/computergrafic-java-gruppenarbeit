package object3d;

import java.awt.Color;

import Vector3D.Vector3D;

public class Sphere extends Object3D {
	//static Color color ;
	
	double radius;
	Vector3D center;
	

	public Sphere( double radius, Vector3D center) {
		super(Color.BLUE);
		
		this.radius=radius;
		this.center = center;
		// TODO Auto-generated constructor stub
	}

	
	//@param eye in formul is the same as origin in method
	//x - points of sphere
	//ray(t )= eye+t*direction
	//|x-center|*|x-center|- radius*radius=<0  formula of sphere  because all the points 
	// but we do not need all the sphere only the points that are visible => the points that are on radius => |x-center|*|x-center|- radius*radius=0 
	// the point of ray meet the points of sphere => x= ray(t )=> |eye+t*direction-center|*|eye+t*direction-center|-radius*radius=0; 
	// danach nach ABC formel wo a  = t*t*direction*direction(kuda delas t poka neponjatno mne), b= 2t*direction*, c = 
	// t ist wie x in ABC formul
	@Override
	public double intersect(Vector3D origin, Vector3D direction) {
		double a= direction.dot(direction);
		System.out.println("a "+a);
		
		Vector3D ec = origin.sub(center);
		
		double b = direction.dot((origin.sub(center)).mul(2.0));
		System.out.println("b "+b);
		
		double c = origin.sub(center).dot(origin.sub(center)) - (this.radius * this.radius);
		System.out.println("c "+c);
		double diskriminante = (b * b) - 4 * a * c;
		

		if (diskriminante > 0.0) {
			System.out.println("discriminante"+diskriminante);
			double t1 = (-b - Math.sqrt(diskriminante)) / (2 * a);
			double t2 = (-b + Math.sqrt(diskriminante)) / (2 * a);
			double t;
			t = Math.min(t1, t2);
			System.out.println("t"+t);
			return t;
			
			}
		return 0;
	}
	
	

}
