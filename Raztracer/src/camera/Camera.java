package camera;
import  Vector3D.*;
import output.Output;

public class Camera {
	//left boarder of the window done in Class Output
	public final  int l = - Output.WIDTH/2;
	
	//right boarder of the window done in Class Output
	public final  int r = l*-1;
	//top boarder of the window done in Class Output
	public final  int t = Output.HEIGHT/2;
	//bottom boarder of the window done in Class Output
	public final  int b =t*-1;
	
	// direction up as coordinates 
	public final  Vector3D UP = new Vector3D(0,1,0);
	// position of camera
	public final  Vector3D eye = new Vector3D (0,0,10);
	
	//direction our Object 
	public final  Vector3D Z = new Vector3D (30,30,30);
	
	
	//direction backwards from the camera napravlenie protivopolzhnoe vektoru view
	public final Vector3D W = eye.sub(Z);
	public final  Normal3 WN = W.asNormal();
	
	

	//direction  from the camera napravlenie vstorony ot napravlenija vzgljada iz kamery
	public final Vector3D U = UP.x(W);
	public final Normal3 UN = U.asNormal();
	
	
	
	//direction upwards from the camera napravlenie VVERX ot napravlenija vzgljada iz kamery
	
	public final Vector3D V = W.x(U);
	public final  Normal3 VN = V.asNormal();
	
	
	//@param d  distance
	public final  double d = t/Math.tan(Math.PI/4)/2;
	
	//abstandberechnen
	public final Vector3D W_mal_d_negated = W.mul(d*-1);
	
	

	

}
